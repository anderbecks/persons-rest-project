package com.example.personsrest.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class PersonEntity implements Person {

    private String id;
    private String name;
    private int age;
    private String city;
    private List<String> groups;


    public PersonEntity(String name, int age, String city, List<String> groups) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.age = age;
        this.city = city;
        this.groups = groups;
    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public void setActive(boolean active) {

    }

    @Override
    public void addGroup(String groupId) {
        this.groups.add(groupId);
    }

    @Override
    public void removeGroup(String groupId) {
        this.groups.remove(groupId);

    }
}
