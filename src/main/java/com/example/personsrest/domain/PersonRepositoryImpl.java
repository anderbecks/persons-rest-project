package com.example.personsrest.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PersonRepositoryImpl implements PersonRepository {

    ArrayList <Person> persons = new ArrayList<>();
    @Override
    public Optional<Person> findById(String id) {
                return Optional.of(persons.stream().filter(p -> p.getId().equals(id)).findFirst().orElse(null));
    }

    @Override
    public List<Person> findAll() {
        return persons;
    }

    @Override
    public Page<Person> findAllByNameContainingOrCityContaining(String name, String city, Pageable pageable) {
        return null;
    }

    @Override
    public void deleteAll() {

    }

    @Override
    public Person save(Person person) {
        persons.add(person);
        return person;
    }

    @Override
    public void delete(String id) {

    }
}
