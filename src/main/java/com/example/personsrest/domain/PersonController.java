package com.example.personsrest.domain;

import com.example.personsrest.remote.GroupRemote;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/persons")
public class PersonController {

    @Autowired
    PersonService personService;

    @Autowired
    GroupRemote groupRemote;

    @GetMapping("/")
    public List<PersonDTO> findAll(){
        return personService.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    @GetMapping("")
    public List<PersonDTO> findPersonsFilteredByName(
            @RequestParam(name = "search") String search,
            @RequestParam(name = "pagenumber") int pagenumber,
            @RequestParam(name = "pagesize") int pagesize){
        System.out.println(search);
        return personService.findAllByNameContainingOrCityContaining(search, pagenumber,pagesize)
                .stream().map(this::toDTO).collect(Collectors.toList());

    }

    @GetMapping("/{id}")
    public PersonDTO findById(@PathVariable String id){
        return toDTO(personService.findById(id).get());
    }

    @PostMapping("/")
    public ResponseEntity<?> createPerson(@RequestBody CreatePerson person){
        return new ResponseEntity<>(toDTO(personService.save(toEntity(person))), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public PersonDTO updatePerson(@PathVariable String id,
                                  @RequestBody UpdatePerson updatePerson){
        return toDTO(personService.updatePerson(id, updatePerson));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePerson(@PathVariable String id){
        personService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/{id}/addGroup/{groupName}")
    public PersonDTO addGroupToPerson(@PathVariable String id, @PathVariable String groupName){
        return toDTO(personService.addGroupToPerson(id,groupName));
    }

    @DeleteMapping("/{personId}/removeGroup/{groupId}")
    public PersonDTO removeGroupFromPerson(@PathVariable String personId, @PathVariable String groupId){
        return toDTO(personService.removeGroupFromPerson(personId,groupId));
    }

    public PersonEntity toEntity(CreatePerson createPerson){
        return new PersonEntity(createPerson.getName(), createPerson.getAge(), createPerson.getCity(), new ArrayList<>());
    }

    public PersonDTO toDTO(Person person) {
        return new PersonDTO(person.getId(),person.getName(), person.getCity(), person.getAge(),
                person.getGroups().stream().map(id -> groupRemote.getNameById(id)).collect(Collectors.toList()));
    }

    @Value
    static class PersonDTO {
        String id;
        String name;
        String city;
        int age;
        List<String> groups;

        @JsonCreator
        public PersonDTO(
                @JsonProperty("id") String id,
                @JsonProperty("name") String name,
                @JsonProperty("city") String city,
                @JsonProperty("age") int age,
                @JsonProperty("groups") List<String> groups) {
            this.id = id;
            this.name = name;
            this.city = city;
            this.age = age;
            this.groups = groups;
        }

    }

    @Value
    static class CreatePerson {
        String name;
        String city;
        int age;
    }

    @Value
    static class UpdatePerson {
        String name;
        String city;
        int age;
    }

}
