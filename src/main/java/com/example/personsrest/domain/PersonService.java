package com.example.personsrest.domain;

import com.example.personsrest.remote.GroupRemote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PersonService {

    ArrayList<Person> persons = new ArrayList<>();

    @Autowired
    PersonRepository personRepository;

    @Autowired
    GroupRemote groupRemote;

    public Optional<Person> findById(String id) {
        return personRepository.findById(id);
    }


    public List<Person> findAll() {
        return personRepository.findAll();
    }


    public Page<Person> findAllByNameContainingOrCityContaining(String search, int pagenumber, int pagesize) {
        Pageable page = PageRequest.of(pagenumber, pagesize);
        return personRepository.findAllByNameContainingOrCityContaining(search,search,page);
    }


    public void deleteAll() {

    }


    public Person save(Person person) {
        return personRepository.save(person);
    }



    public void delete(String id) {
        personRepository.delete(id);
    }

    public Person updatePerson(String id, PersonController.UpdatePerson updatePerson) {
            Person person = personRepository.findById(id).orElse(null);
            if (person != null){
            person.setName(updatePerson.getName());
            person.setCity(updatePerson.getCity());
            person.setAge(updatePerson.getAge());
            return personRepository.save(person);
            }
            else {return null;}
    }

    public Person addGroupToPerson(String id, String groupName) {
        Person person = personRepository.findById(id).orElse(null);
        String groupId = groupRemote.createGroup(groupName);
        person.addGroup(groupId);
            return personRepository.save(person);

    }

    public Person removeGroupFromPerson(String personId, String groupId) {
        Person person = personRepository.findById(personId).orElse(null);
        if (person !=null){
            if(groupId.length() == 36){
                person.removeGroup(groupId);
            } else {
                person.getGroups().removeIf(g -> groupRemote.getNameById(g).equals(groupId));
            }
            return personRepository.save(person);
        }
        else {return null;}
    }
}

